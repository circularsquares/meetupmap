# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

event = Event.new(
  :name => 'pickup soccer', 
  :location => [40.6830069, -73.9800645], 
  :event_url => 'http://www.meetup.com', 
  :description => 'come have fun')
  
event.save

event = Event.new(
  :name => 'hackathon', 
  :location => [40.682981227710584, -73.97335052490234], 
  :event_url => 'http://www.meetup2.com', 
  :description => 'programming!')

event.save

cat = Category.new(
  :name => 'HTML5 Programming',
  :shortname => 'html5programming'
)

cat.save

cat = Category.new(
  :name => 'Soccer',
  :shortname => 'soccer'
)

cat.save