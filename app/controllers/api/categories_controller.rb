class Api::CategoriesController < ApplicationController

  # /api/categories.json
  def index
    @categories = Category.sort(:name.asc)
  end
end
