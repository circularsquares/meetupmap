class Api::EventsController < ApplicationController
  before_filter :set_scope

  def index
    sw_point = [params[:sw_lat].to_f, params[:sw_lon].to_f]
    ne_point = [params[:ne_lat].to_f, params[:ne_lon].to_f]
    @events = @scope.in_bounds(sw_point, ne_point).limit(200)
    @events = Event.all
  end

  protected
  def set_scope
    if params[:category_id]
      @scope = Category.find_by_shortname!(params[:category_id]).events
    else
      @scope = Event
    end
  end
end
