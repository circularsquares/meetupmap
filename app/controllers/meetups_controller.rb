class MeetupsController < ApplicationController

  def index
  end

  def scrape
    client = MeetupApiClient.new('19225767452531414117141494022')

    Category.destroy_all

    client.categories['results'].each do |category|
      c = Category.new(:name      => category["name"],
                       :shortname => category["shortname"])
      c.save!
    end

    @categories = Category.all

    #Topic.destroy_all
    #fetch_all(client, :topics, :search => 'tech') do |topic|
    #  save_topic(topic)
    #end

    Event.destroy_all
    Topic.all.each do |topic|
      fetch_all(client, :events, :topic => topic.urlkey) do |event|
        save_event(event)
      end
    end
  end

  def fetch_all(client, entity, options = nil)
    options ||= {}
    offset = 0
    total = 1
    options[:offset] = offset

    while offset < total
      result = client.send(entity, options)

      result['results'].each do |item|
        yield item
      end

      offset += result['meta']['count']
      total = result['meta']['total_count']
    end

  end

  def save_event(data)
    event = Event.new(
      :_id         => clean_encoding(data['id']),
      :name        => clean_encoding(data['name']),
      :status      => clean_encoding(data['status']),
      :utc_offset  => data['utc_offset'],
      :duration    => data['duration'],
      :distance    => data['distance'],
      :time        => Time.at(data['time'] / 1000),
      :event_url   => clean_encoding(data['event_url']),
      :description => clean_encoding(data['description']),
      :location    => data['venue'] && [data['venue']['lon'],
                                        data['venue']['lat']],
      )
    event.save!

    if Group.where(:id => data['group']['id']).count < 1
      save_group(data['group'])
    end
  end

  def save_group(data)
    group = Group.new(
      :_id  => data['id'],
      :name => clean_encoding(data['name']),
      :who  => clean_encoding(data['who']),
    )
    group.save!
  end

  def save_topic(data)
    topic = Topic.new(
      :_id     => clean_encoding(data['id']),
      :name    => clean_encoding(data['name']),
      :urlkey  => clean_encoding(data['urlkey']),
      :link    => clean_encoding(data['link']),
    )
    topic.save!
  end

  def clean_encoding(s)
    s.encode('UTF-16', 'UTF-8',
             :invalid => :replace,
             :replace => '').encode('UTF-8', 'UTF-16')
  end
end
