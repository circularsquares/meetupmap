json.array!(@events) do |json, event|
  json.(event, :name, :location, :description)
end
