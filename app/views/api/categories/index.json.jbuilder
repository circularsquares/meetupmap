json.array!(@categories) do |json, category|
	json.(category, :id, :name, :shortname)
end
