class Category
  include MongoMapper::Document

  key :name,      String
  key :shortname, String

  many :events
end
