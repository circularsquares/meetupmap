class Event
  include MongoMapper::Document

  key :name,        String
  key :status,      String
  key :utc_offset,  Integer
  key :duration,    Integer
  key :distance,    Float
  key :time,        Time
  key :event_url,   String
  key :description, String
  key :location,    Array

  ensure_index [[:location, '2d']]

  def self.in_bounds(sw_point, ne_point)
    box = [sw_point, ne_point]
    where(:location => {'$within' => {'$box' => box}})
  end
end
