class Group
  include MongoMapper::Document

  key :name, String
  key :who,  String
end
