class Topic
  include MongoMapper::Document

  key :name,    String
  key :urlname, String
  key :link,    String
end
