// Category < Model
// Represents a single category item, found in the dropdown menu in sidebar

var Category = Backbone.Model.extend({
  defaults: {
    name: '--name--',
    shortname: '--shortname--'
  }
});

// Event < Model
// Represents a single meetup event
var Event = Backbone.Model.extend({
  defaults: {
    name: '',
    status: '',
    utc_offset: 0,
    duration: 0, 
    distance: 0.0,
    time: 0,
    event_url: '',
    description: '',
    location: [],
  }
});

// Categories < Collection
// Collection of categories that a meetup event is associated with
var Categories = Backbone.Collection.extend({
  model: Category,
  url: '/api/categories.json',
  initialize: function() {
    var self = this;
  },
  parse: function(data) {
    console.log('parse',data);
    return data;
  }
});

// Events < Collection
// Collection of meetup events; filtering is done later in the fetch() call
var Events = Backbone.Collection.extend({
  url: '/api/events.json'
});

// Sidebar < View
// Represents the sidebar, which contains category, location, and topic filtering
var Sidebar = Backbone.View.extend({
  el: $('#sidebar'),
  initialize: function() {
    console.log('init view');
    this.collection.bind("reset",this.render, this); // reset gets called after a fetch
    this.collection.fetch();
  },
  events: {
    'click a': 'testHandler',
    'change select': 'handleCategoryChange'
  },
  handleCategoryChange: function(e) {
    console.log($(e.target).val());
    this.options.eventMap.fetchWithBounds($(e.target).val());
  },
  testHandler: function(evt) {
    evt.preventDefault();
    alert('test');
  },
  render: function() { console.log('collection', this.collection.toJSON());
    var source   = $("#sidebar-template").html();
    var template = Handlebars.compile(source);
    var html = template({"categories": this.collection.toJSON()});
    this.$el.html(html);
  }
});

// EventMap < View
// View for the bigass map, if that wasn't completely obvious...
var EventMap = Backbone.View.extend({
  el: $('#map_canvas'),
  map: undefined,
  bounds: undefined,
  markers: [],
  mapOptions: {
    zoom: 15,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    panControl: false,
    scaleControlOptions: {
      position: google.maps.ControlPosition.TOP_RIGHT
    },
    mapTypeId: google.maps.MapTypeId.ROADMAP
  },

  initialize: function() {
    var self = this;
    this.map = new google.maps.Map(this.el, this.mapOptions);

    // get a new batch of events to show on the map
    this.collection = new Events();
    this.collection.bind("reset",this.render, this); // reset gets called after a fetch

    // this gets fired regardless of whether we can geo-detect
    var onMapReadyHandler = function(lat, lng) {
      self.setMapPosition(lat, lng);
      google.maps.event.addListener(self.map, 'idle', function() {
        console.log('idle');
        self.fetchWithBounds();
      });
    };
    
    // geolocation
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        console.log('geo');
        onMapReadyHandler(position.coords.latitude,position.coords.longitude);
      });
    } else {
      onMapReadyHandler(-34.397, 150.644);
    }
    
  },

  setMapPosition: function(lat, lng) {
    this.map.setCenter(new google.maps.LatLng(lat, lng));
  },

  fetchWithBounds: function(category) {
    this.clearMarkers();
    var bounds = this.map.getBounds();
    
    var params = {
      sw_lat: bounds.getSouthWest().lat(),
      sw_lon: bounds.getSouthWest().lng(),
      ne_lat: bounds.getNorthEast().lat(),
      ne_lon: bounds.getNorthEast().lng()
    };
    
    if(category) {
      params.category_id = category
    }
    
    this.collection.fetch({
      data: $.param(params)
    });
  },

  clearMarkers: function() {
    _.each(this.markers, function(marker) {
      marker.setMap(null);
      marker = null;
    });
  },

  render: function(data) {
    var events = data.toJSON();
    var self = this;
    _.each(events, function(data) {
      var options = {
        position: new google.maps.LatLng(data.location[0], data.location[1]),
        map: self.map,
        title: data.name
      };
      var marker = new google.maps.Marker(options);
      self.markers.push(marker);
    });
    console.log('eventmap data', data.toJSON());
  }
});

// AppRouter < Router
// Not sure if there is a "right way" to do this
var AppRouter = Backbone.Router.extend({
  routes: {
    '': 'index'
  },
  
  initialize: function() {
  },
  
  index: function() {
    console.log('start');
    var mm = new EventMap;
    new Sidebar({
      collection: new Categories,
      eventMap: mm
    });
  }
});

// kick this thing off
new AppRouter;
Backbone.history.start()
