require 'net/http'

class MeetupApiError < RuntimeError
end


class MeetupApiClient

  def initialize(key)
    @key = key
    @url = 'http://api.meetup.com'
  end

  def categories
    result = make_request('/2/categories')
  end

  def events(options = nil)
    options ||= {}
    make_request('/2/open_events', options)
  end

  def topics(options = nil)
    options ||= {}
    make_request('/topics', options)
  end

  protected

    def make_request(path, parameters = nil)
      parameters ||= {}
      parameters[:key] = @key
      query = build_url(parameters)
      url = URI.parse(@url + path + query)

      result = Net::HTTP.get_response(url).body
      data = JSON.parse(result)

      if data['problem']
        raise MeetupApiError.new(data['problem'] + '. ' +
                                 data['details'])
      end

      data
    end

    def build_url(options)
      options = encode_options(options)
      params_for(options)
    end

    # Encode a hash of options to be used as request parameters
    def encode_options(options)
      options.each do |key,value|
        options[key] = URI.encode(value.to_s)
      end
    end

    # Create a query string from an options hash
    def params_for(options)
      params = []
      options.each do |key, value|
        params << "#{key}=#{value}"
      end
      params.length ? "?#{params.join("&")}" : ''
    end
end
